# sediment_plumes

Small repository with test tools for working with Sentinel-2 and Sentinel-3 data for river plume identification and extent tracking.

To retrieve this repository:
* install Anaconda 
* from either from the terminal (Linux/OSx) or Anaconda Navigator Powershell prompt type `git clone https://gitlab.com/benloveday/sediment_plumes.git`
* create the required environment using `conda env create -f environment.yml`
* activate the environment in the terminal (`conda activate sediment_plumes`) or inside Anaconda Navigator
* Launch JupyterLab using `jupyter lab` from the prompt or via the Anaconda Navigator
`