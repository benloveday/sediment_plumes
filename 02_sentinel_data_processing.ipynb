{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "8a0cc874-656f-4386-9fa3-02fb25ba6b2b",
   "metadata": {},
   "source": [
    "# Sentinel Data Procesing\n",
    "\n",
    "The data we have downloaded are L1B top of atmosphere products. These have not been atmospherically corrected and may be missing some crucial information on masks, as well as some of the products we may want to create a synthetic RGB image for analysis. This notebook will handle all of these aspects for both Sentinel-3 and Sentinel-2.\n",
    "\n",
    "The core of this processing is the <a href=\"https://step.esa.int/main/download/snap-download/\n",
    "\" target=\"_blank\">SNAP</a> package, and more specifically, its automated Graph Builder Tool (or GPT). You will need to install SNAP on your local system, and know where the `gpt` executable is in order to run this script.\n",
    "\n",
    "Once you have installed SNAP, you need to open it in GUI mode, open the plugins section and install the IdePix OLCI and IdePix S2-MIS plugins (as shown below)\n",
    "\n",
    "<img src='./img/IdePix.png' align='right' width='100%'/>\n",
    "\n",
    "Once we have done that, we can call GPT from the command line without having to open SNAP again. This allows us to effectively and consistently batch process a lot of images. To do this processing, we need to define a couple of XML workflows, which SNAP will read and follow. You can find examples for S2 and S3 in the `gpt_templates` folder. All of the magic is here! If you want to learn more about what `gpt` can do, then you can always type <br><br> `/Applications/snap/bin/gpt -h` *(or similar depending on your system, see below)*.\n",
    "\n",
    "You can ask for help on individual methods using, for example; <br><br> `/Applications/snap/bin/gpt subset -h`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7d66b790-55d7-4090-9eab-1f44707c3e62",
   "metadata": {},
   "source": [
    "## Step 1: Import all the python packages we need.\n",
    "\n",
    "This repository contains an \"environment.yml\" file which will build the conda environment for you. This contains all the packages listed below. To use it, you should make sure that you have the latest version of <a href=\"https://anaconda.org/\" target=\"_blank\">Anaconda</a> installed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "5c80d9c2-25d1-486c-a856-ba00670a92ac",
   "metadata": {},
   "outputs": [],
   "source": [
    "# import packages\n",
    "import os           # a library that allows us access to basic operating system commands like making directories\n",
    "import shutil       # a library that allows us access to basic operating system commands like copy\n",
    "import subprocess   # a library that lets us call external processes\n",
    "import fnmatch      # a library that supports comparing files to a specified pattern\n",
    "import platform     # a library that determines the current operating system\n",
    "from shapely import geometry # a library that support construction of geometry objects"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bd76d5a1-a2c2-45db-8edf-6bf996c0baac",
   "metadata": {},
   "source": [
    "## Step 2: create an output directory for our products\n",
    "\n",
    "This is where we will store anything we create."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "e75a592e-b7f3-4af7-9bb0-9bddc06b905a",
   "metadata": {},
   "outputs": [],
   "source": [
    "output_dir = os.path.join(os.getcwd(), \"outputs\")\n",
    "os.makedirs(output_dir, exist_ok=True)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "41542401-4355-4b76-b7f9-459e7c0d62b3",
   "metadata": {},
   "source": [
    "## Step 3: find the GPT executable\n",
    "\n",
    "We need to tell the notebook where GPT is. The cell below should give some guidance, based on each operating system, but you may need to adapt it make sure everything is correct."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "f7bf277c-db1a-4da3-8bc7-cf116a71ec54",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "This platform is: darwin\n",
      "The default path is /Applications/snap/bin/gpt, please adapt the GPT_PATH variable if this is not correct\n"
     ]
    }
   ],
   "source": [
    "print(f\"This platform is: {platform.system().lower()}\")\n",
    "if platform.system().lower() == \"darwin\":\n",
    "    GPT_PATH = os.path.join(\"/\", \"Applications\", \"snap\", \"bin\", \"gpt\")\n",
    "elif platform.system().lower() == \"windows\":\n",
    "    GPT_PATH = os.path.join(\"C:/\", \"Users\", \"<YOUR USER NAME>\", \"AppData\", \"local\", \"Programs\", \"snap\", \"bin\", \"gpt.exe\")\n",
    "print(f\"The default path is {GPT_PATH}, please adapt the GPT_PATH variable if this is not correct\")"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "df6c1298-8a7c-4a6a-a8a4-df91b41262d6",
   "metadata": {},
   "source": [
    "## Step 4: select the template we want to use\n",
    "\n",
    "The templates contain all the workflow information we need to apply our methodology to a Sentinel-3 or Sentinel-2 files. I have created an example for each."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "0cf3ab83-2168-4e29-b1c2-b252b7b67ebc",
   "metadata": {},
   "outputs": [],
   "source": [
    "template_xml_S3 = os.path.join('gpt_templates',\n",
    "                               'GPT_config_template_subset_idepix_c2rcc_S3.xml')\n",
    "template_xml_S2 = os.path.join('gpt_templates',\n",
    "                               'GPT_config_template_subset_idepix_c2rcc_S2.xml')\n",
    "\n",
    "my_template_xml = template_xml_S2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "63f175d0-e93e-41e9-aa4a-b49b1f88d2a2",
   "metadata": {},
   "source": [
    "## Step 5: select the data we want to use\n",
    "\n",
    "Now lets find our data. I'll be a bit clever here and select data based on the template we are using."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "5580f687-fb77-4932-8205-98c7fe29aa50",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "/Users/benloveday/Code/Git_Reps/sediment_plumes/products/S2B_MSIL1C_20230817T112119_N0509_R037_T29TNF_20230817T132408.SAFE/MTD_MSIL1C.xml\n"
     ]
    }
   ],
   "source": [
    "# choose the directory to search\n",
    "input_dir = os.path.join(os.getcwd(),'products')\n",
    "\n",
    "# make a list of all the input files in your input directory\n",
    "input_files=[]\n",
    "for root, _, filenames in os.walk(input_dir):\n",
    "    for filename in fnmatch.filter(filenames, '*xfdumanifest.xml'):\n",
    "        if \"EFR\" in root and \"S3\" in my_template_xml:\n",
    "            input_files.append(os.path.join(root, filename))\n",
    "    for filename in fnmatch.filter(filenames, '*MTD_MSIL1C.xml'):    \n",
    "        if \"MSIL1C\" in root and \"S2\" in my_template_xml:\n",
    "            input_files.append(os.path.join(root, filename))\n",
    "\n",
    "# and show the list        \n",
    "for input_file in input_files:\n",
    "    print(input_file)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "48eec37f-2adf-4adf-bae8-46c687f1beb6",
   "metadata": {},
   "source": [
    "## Step 6: customise the template\n",
    "\n",
    "We will update our template with this information \"on the fly\", meaning that we can adapt hings in the notebook withough making changes anywhere else. The following information will be updated each time we run;\n",
    "* the ROI\n",
    "* the input product name\n",
    "* the output product name"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "51f8b2d1-1d9e-4904-8cb2-04bf5c9ba327",
   "metadata": {},
   "outputs": [],
   "source": [
    "# set geo_region to subset\n",
    "# space/time filter the collection for products\n",
    "\n",
    "north = 41.3\n",
    "west = -8.8\n",
    "south = 40.9\n",
    "east = -8.50\n",
    "\n",
    "ROI = [[west, south], [east, south], [east, north], [west, north], [west, south]]\n",
    "ROI_polygon = geometry.Polygon([[p[0], p[1]] for p in ROI])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2374e1e3-500e-43b9-9e94-693a7081f34f",
   "metadata": {},
   "source": [
    "## Step 7: running the processing\n",
    "\n",
    "So, now lets run! We will cycle over our input files, adapt the output name from the input name, and add the ROI for each iteration. This could take some time depending on how big our ROI is, so pull up a chair.....(if it runs nearly immediately, something is probably wrong).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "a3c34246-3212-4d4a-b12b-eacfdd344871",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "-------------- Processing: --------------\n",
      "/Users/benloveday/Code/Git_Reps/sediment_plumes/products/S2B_MSIL1C_20230817T112119_N0509_R037_T29TNF_20230817T132408.SAFE/MTD_MSIL1C.xml\n",
      "-- To: --\n",
      "S2B_MSIL1C_20230817T112119_N0509_R037_T29TNF_20230817T132408.SAFE\n",
      "-- Generating config: --\n",
      "/Users/benloveday/Code/Git_Reps/sediment_plumes/outputs/run_config.xml\n",
      "-- Config ready; running: --\n",
      "/Applications/snap/bin/gpt /Users/benloveday/Code/Git_Reps/sediment_plumes/outputs/run_config.xml\n",
      "----- Finished running this product -----\n",
      "\n"
     ]
    }
   ],
   "source": [
    "# MAIN: the loop goes over each input file in the input_files list\n",
    "for input_file in input_files:\n",
    "\n",
    "    # 1. define an output file name. This is derived from the input file, but with a _SUBSET_IDEPIX_C2RCC suffix.\n",
    "    output_file = input_file.replace(input_dir,output_dir)\n",
    "    output_file = os.path.dirname(output_file).replace('.SEN3','_SUBSET_IDEPIX_C2RCC.nc')\n",
    "    print(f\"-------------- Processing: --------------\\n{input_file}\")\n",
    "    print(f\"-- To: --\\n{os.path.basename(output_file)}\")\n",
    "    \n",
    "    # 2. read the template xml and adapt it for the current granule\n",
    "    my_config = os.path.join(output_dir, 'run_config.xml')\n",
    "    print(f\"-- Generating config: --\\n{my_config}\")\n",
    "    shutil.copy(my_template_xml, my_config)\n",
    "\n",
    "    with open(my_template_xml, 'r') as file:\n",
    "        filedata = file.read()\n",
    "        \n",
    "    # Replace the target strings\n",
    "    filedata = filedata.replace('SOURCE_PRODUCT', input_file)\n",
    "    filedata = filedata.replace('OUTPUT_PRODUCT', output_file)\n",
    "    filedata = filedata.replace('GEO_REGION', str(ROI_polygon))\n",
    "\n",
    "    # Write the file out again\n",
    "    with open(my_config, 'w') as file:\n",
    "        file.write(filedata)\n",
    "\n",
    "    # 3. the processing call is a follows below.\n",
    "    c2rcc_processing_call = GPT_PATH + ' ' + my_config\n",
    "    \n",
    "    # It is useful to check that the command call is correct before launching the call\n",
    "    print(f\"-- Config ready; running: --\\n{c2rcc_processing_call}\")\n",
    "    \n",
    "    # Run the gpt command\n",
    "    process = subprocess.Popen(c2rcc_processing_call, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)\n",
    "    output, err = process.communicate()\n",
    "\n",
    "    print(f\"----- Finished running this product -----\\n\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.9.17"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
